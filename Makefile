all:
	win_flex --outfile=lexer.cpp lexer.l
	bison -y -d -o  grammar.cpp grammar.y
	g++  main.cpp Document.cpp grammar.cpp lexer.cpp Attribute.cpp Tag.cpp utils.cpp -o parser

clear: 
	rm parser.exe