#pragma once
#include "attribute.h"

using namespace std;

class Tag
{
private:
	string name;
	vector<Attribute> attribute_list;
	vector<string> valid_attribute_names_list;
	size_t line;
	size_t column;

	const db_attr_type &attribute_database;
	const db_tag_type &tag_database;

	bool check_name();
	bool check_attr_name(string name);
	void init();

public:
	Tag(string nm, const db_attr_type &attr_db, const db_tag_type &tag_db, size_t _line, size_t _column)
		: name(nm), attribute_database(attr_db), tag_database(tag_db), line(_line), column(_column)
	{ 
		init(); 
	}

	string get_name()    { return name; }
	size_t GetLine()    { return line; }
	size_t GetColumn()  { return column; }
	void AddAttribute(string name, string val);
};