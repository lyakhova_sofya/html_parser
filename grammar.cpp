#define YYBISON 1

#define	OPENING_TAG_BRACKET	258
#define	CLOSING_TAG_BRACKET	259
#define	SLASH	260
#define	ASSIGNMENT	261
#define	DOCTYPE	262
#define	DOCTYPE_HTML_5	263
#define	DOCTYPE_XHTML	264
#define	DOCTYPE_STRICT_1	265
#define	DOCTYPE_STRICT_2	266
#define	DOCTYPE_TRANS_1	267
#define	DOCTYPE_TRANS_2	268
#define	DOCTYPE_FRAME_1	269
#define	DOCTYPE_FRAME_2	270
#define	DOCTYPE_XHTML_STRICT_1	271
#define	DOCTYPE_XHTML_STRICT_2	272
#define	DOCTYPE_XHTML_TRANS_1	273
#define	DOCTYPE_XHTML_TRANS_2	274
#define	DOCTYPE_XHTML_FRAME_1	275
#define	DOCTYPE_XHTML_FRAME_2	276
#define	DOCTYPE_XHTML_1_1_1	277
#define	DOCTYPE_XHTML_1_1_2	278
#define	DOUBLE_QUOTE_STRING	279
#define	SINGLE_QUOTE_STRING	280
#define	TEXT	281

#line 1 "grammar.y"

	#include <iostream>
    #include <string>
	#include "Document.h"
    #include "utils.h"
    #include "debug.h"

    #pragma warning( disable:4996 )
    #pragma warning( disable:5033 )
	
    int  yyerror (const char * err);
    int  yylex ();
	extern FILE *yyin;
    extern int yylineno;
    extern int g_LexerColumn;
    extern Document doc;

#line 19 "grammar.y"
typedef union
{
    void *string_t;
} YYSTYPE;

#ifndef YYLTYPE
typedef
  struct yyltype
    {
      int timestamp;
      int first_line;
      int first_column;
      int last_line;
      int last_column;
      char *text;
   }
  yyltype;

#define YYLTYPE yyltype
#endif

#include <stdio.h>

#ifndef __cplusplus
#ifndef __STDC__
#define const
#endif
#endif



#define	YYFINAL		69
#define	YYFLAG		-32768
#define	YYNTBASE	27

#define YYTRANSLATE(x) ((unsigned)(x) <= 281 ? yytranslate[x] : 39)

static const char yytranslate[] = {     0,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     1,     2,     3,     4,     5,
     6,     7,     8,     9,    10,    11,    12,    13,    14,    15,
    16,    17,    18,    19,    20,    21,    22,    23,    24,    25,
    26
};

#if YYDEBUG != 0
static const short yyprhs[] = {     0,
     0,     1,     4,     6,     8,    12,    18,    24,    30,    36,
    42,    48,    54,    56,    59,    61,    64,    66,    68,    70,
    75,    79,    84,    90,    96,   101,   103,   106,   110,   112,
   114,   116,   118,   120,   122,   124,   126,   128,   130,   132
};

static const short yyrhs[] = {    -1,
    28,    29,     0,    29,     0,    28,     0,     3,     8,     4,
     0,     3,     7,    10,    11,     4,     0,     3,     7,    12,
    13,     4,     0,     3,     7,    14,    15,     4,     0,     3,
     9,    16,    17,     4,     0,     3,     9,    18,    19,     4,
     0,     3,     9,    20,    21,     4,     0,     3,     9,    22,
    23,     4,     0,    30,     0,    29,    30,     0,    37,     0,
    29,    37,     0,    31,     0,    32,     0,    33,     0,     3,
    26,    34,     4,     0,     3,    26,     4,     0,     3,     5,
    26,     4,     0,     3,     5,    26,    38,     4,     0,     3,
    26,    34,     5,     4,     0,     3,    26,     5,     4,     0,
    35,     0,    34,    35,     0,    26,     6,    36,     0,    26,
     0,    25,     0,    24,     0,    26,     0,     5,     0,     6,
     0,     5,     0,    26,     0,    25,     0,    24,     0,    26,
     0,    38,    26,     0
};

#endif

#if YYDEBUG != 0
static const short yyrline[] = { 0,
    63,    64,    65,    66,    70,    71,    72,    73,    74,    75,
    76,    77,    81,    82,    83,    84,    88,    89,    90,    94,
    95,    99,   100,   104,   105,   109,   110,   114,   115,   119,
   120,   121,   122,   126,   127,   128,   129,   130,   134,   135
};

static const char * const yytname[] = {   "$","error","$undefined.","OPENING_TAG_BRACKET",
"CLOSING_TAG_BRACKET","SLASH","ASSIGNMENT","DOCTYPE","DOCTYPE_HTML_5","DOCTYPE_XHTML",
"DOCTYPE_STRICT_1","DOCTYPE_STRICT_2","DOCTYPE_TRANS_1","DOCTYPE_TRANS_2","DOCTYPE_FRAME_1",
"DOCTYPE_FRAME_2","DOCTYPE_XHTML_STRICT_1","DOCTYPE_XHTML_STRICT_2","DOCTYPE_XHTML_TRANS_1",
"DOCTYPE_XHTML_TRANS_2","DOCTYPE_XHTML_FRAME_1","DOCTYPE_XHTML_FRAME_2","DOCTYPE_XHTML_1_1_1",
"DOCTYPE_XHTML_1_1_2","DOUBLE_QUOTE_STRING","SINGLE_QUOTE_STRING","TEXT","begin",
"htmlDoctype","htmlDocument","htmlElement","htmlTagOpen","htmlTagClose","htmlTagSingle",
"htmlAttributeList","htmlAttribute","htmlAttributeValue","htmlContent","htmlText",
""
};
#endif

static const short yyr1[] = {     0,
    27,    27,    27,    27,    28,    28,    28,    28,    28,    28,
    28,    28,    29,    29,    29,    29,    30,    30,    30,    31,
    31,    32,    32,    33,    33,    34,    34,    35,    35,    36,
    36,    36,    36,    37,    37,    37,    37,    37,    38,    38
};

static const short yyr2[] = {     0,
     0,     2,     1,     1,     3,     5,     5,     5,     5,     5,
     5,     5,     1,     2,     1,     2,     1,     1,     1,     4,
     3,     4,     5,     5,     4,     1,     2,     3,     1,     1,
     1,     1,     1,     1,     1,     1,     1,     1,     1,     2
};

static const short yydefact[] = {     1,
     0,    35,    34,    38,    37,    36,     4,     3,    13,    17,
    18,    19,    15,     0,     0,     0,     0,     0,     0,     2,
    14,    16,     0,     0,     0,     0,     5,     0,     0,     0,
     0,    21,     0,    29,     0,    26,    22,    39,     0,     0,
     0,     0,     0,     0,     0,     0,    25,     0,    20,     0,
    27,    23,    40,     6,     7,     8,     9,    10,    11,    12,
    33,    31,    30,    32,    28,    24,     0,     0,     0
};

static const short yydefgoto[] = {    67,
     7,     8,     9,    10,    11,    12,    35,    36,    65,    13,
    39
};

static const short yypact[] = {    -3,
    10,-32768,-32768,-32768,-32768,-32768,     1,     1,-32768,-32768,
-32768,-32768,-32768,   -25,    37,    20,    26,     4,    11,     1,
-32768,-32768,     8,    -6,    16,    -1,-32768,    21,    31,     7,
    29,-32768,    49,    48,     6,-32768,-32768,-32768,     9,    51,
    52,    53,    54,    55,    56,    57,-32768,    15,-32768,    58,
-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,
-32768,-32768,-32768,-32768,-32768,-32768,    63,    64,-32768
};

static const short yypgoto[] = {-32768,
-32768,    59,    23,-32768,-32768,-32768,-32768,    30,-32768,    25,
-32768
};


#define	YYLAST		66


static const short yytable[] = {     1,
    23,     2,     3,    19,    40,     2,     3,    32,    33,    49,
    50,    37,    52,    42,    14,    14,    15,    16,    17,    61,
     4,     5,     6,    27,     4,     5,     6,    45,    41,    34,
    21,    34,    22,    38,    53,    18,    18,    43,    62,    63,
    64,    28,    21,    29,    22,    30,    24,    31,    25,    44,
    26,    46,    47,    48,    54,    55,    56,    57,    58,    59,
    60,    66,    68,    69,    51,    20
};

static const short yycheck[] = {     3,
    26,     5,     6,     3,    11,     5,     6,     4,     5,     4,
     5,     4,     4,    15,     5,     5,     7,     8,     9,     5,
    24,    25,    26,     4,    24,    25,    26,    21,    13,    26,
     8,    26,     8,    26,    26,    26,    26,    17,    24,    25,
    26,    16,    20,    18,    20,    20,    10,    22,    12,    19,
    14,    23,     4,     6,     4,     4,     4,     4,     4,     4,
     4,     4,     0,     0,    35,     7
};

#line 3 "bison.simple"

#ifndef alloca
#ifdef __GNUC__
#define alloca __builtin_alloca
#else /* not GNU C.  */
#if (!defined (__STDC__) && defined (sparc)) || defined (__sparc__) || defined (__sparc) || defined (__sgi)
#include <alloca.h>
#else /* not sparc */
#if defined (MSDOS) && !defined (__TURBOC__)
#include <malloc.h>
#else /* not MSDOS, or __TURBOC__ */
#if defined(_AIX)
#include <malloc.h>
 #pragma alloca
#else /* not MSDOS, __TURBOC__, or _AIX */
#ifdef __hpux
#ifdef __cplusplus
extern "C" {
void *alloca (unsigned int);
};
#else /* not __cplusplus */
void *alloca ();
#endif /* not __cplusplus */
#endif /* __hpux */
#endif /* not _AIX */
#endif /* not MSDOS, or __TURBOC__ */
#endif /* not sparc.  */
#endif /* not GNU C.  */
#endif

#define yyerrok		(yyerrstatus = 0)
#define yyclearin	(yychar = YYEMPTY)
#define YYEMPTY		-2
#define YYEOF		0
#define YYACCEPT	return(0)
#define YYABORT 	return(1)
#define YYERROR		goto yyerrlab1

#define YYFAIL		goto yyerrlab
#define YYRECOVERING()  (!!yyerrstatus)
#define YYBACKUP(token, value) \
do								\
  if (yychar == YYEMPTY && yylen == 1)				\
    { yychar = (token), yylval = (value);			\
      yychar1 = YYTRANSLATE (yychar);				\
      YYPOPSTACK;						\
      goto yybackup;						\
    }								\
  else								\
    { yyerror ("syntax error: cannot back up"); YYERROR; }	\
while (0)

#define YYTERROR	1
#define YYERRCODE	256

#ifndef YYPURE
#define YYLEX		yylex()
#endif

#ifdef YYPURE
#ifdef YYLSP_NEEDED
#ifdef YYLEX_PARAM
#define YYLEX		yylex(&yylval, &yylloc, YYLEX_PARAM)
#else
#define YYLEX		yylex(&yylval, &yylloc)
#endif
#else /* not YYLSP_NEEDED */
#ifdef YYLEX_PARAM
#define YYLEX		yylex(&yylval, YYLEX_PARAM)
#else
#define YYLEX		yylex(&yylval)
#endif
#endif /* not YYLSP_NEEDED */
#endif

#ifndef YYPURE

int	yychar;			/*  the lookahead symbol		*/
YYSTYPE	yylval;			/*  the semantic value of the		*/
				/*  lookahead symbol			*/

#ifdef YYLSP_NEEDED
YYLTYPE yylloc;			/*  location data for the lookahead	*/
				/*  symbol				*/
#endif

int yynerrs;			/*  number of parse errors so far       */
#endif  /* not YYPURE */

#if YYDEBUG != 0
int yydebug;			/*  nonzero means print parse trace	*/
/* Since this is uninitialized, it does not stop multiple parsers
   from coexisting.  */
#endif

#ifndef	YYINITDEPTH
#define YYINITDEPTH 200
#endif

#if YYMAXDEPTH == 0
#undef YYMAXDEPTH
#endif

#ifndef YYMAXDEPTH
#define YYMAXDEPTH 10000
#endif

#ifdef __GNUC__
int yyparse (void);
#endif

#if __GNUC__ > 1		/* GNU C and GNU C++ define this.  */
#define __yy_memcpy(FROM,TO,COUNT)	__builtin_memcpy(TO,FROM,COUNT)
#else				/* not GNU C or C++ */
#ifndef __cplusplus

static void
__yy_memcpy (from, to, count)
     char *from;
     char *to;
     int count;
{
  register char *f = from;
  register char *t = to;
  register int i = count;

  while (i-- > 0)
    *t++ = *f++;
}

#else
static void
__yy_memcpy (char *from, char *to, int count)
{
  register char *f = from;
  register char *t = to;
  register int i = count;

  while (i-- > 0)
    *t++ = *f++;
}

#endif
#endif

#line 192 "bison.simple"

#ifdef YYPARSE_PARAM
#define YYPARSE_PARAM_DECL void *YYPARSE_PARAM;
#else
#define YYPARSE_PARAM
#define YYPARSE_PARAM_DECL
#endif

int
yyparse(YYPARSE_PARAM)
     YYPARSE_PARAM_DECL
{
  register int yystate;
  register int yyn;
  register short *yyssp;
  register YYSTYPE *yyvsp;
  int yyerrstatus;	/*  number of tokens to shift before error messages enabled */
  int yychar1 = 0;		/*  lookahead token as an internal (translated) token number */

  short	yyssa[YYINITDEPTH];	/*  the state stack			*/
  YYSTYPE yyvsa[YYINITDEPTH];	/*  the semantic value stack		*/

  short *yyss = yyssa;		/*  refer to the stacks thru separate pointers */
  YYSTYPE *yyvs = yyvsa;	/*  to allow yyoverflow to reallocate them elsewhere */

#ifdef YYLSP_NEEDED
  YYLTYPE yylsa[YYINITDEPTH];	/*  the location stack			*/
  YYLTYPE *yyls = yylsa;
  YYLTYPE *yylsp;

#define YYPOPSTACK   (yyvsp--, yyssp--, yylsp--)
#else
#define YYPOPSTACK   (yyvsp--, yyssp--)
#endif

  int yystacksize = YYINITDEPTH;

#ifdef YYPURE
  int yychar;
  YYSTYPE yylval;
  int yynerrs;
#ifdef YYLSP_NEEDED
  YYLTYPE yylloc;
#endif
#endif

  YYSTYPE yyval;
  int yylen;

#if YYDEBUG != 0
  if (yydebug)
    fprintf(stderr, "Starting parse\n");
#endif

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY;		/* Cause a token to be read.  */

  yyssp = yyss - 1;
  yyvsp = yyvs;
#ifdef YYLSP_NEEDED
  yylsp = yyls;
#endif
yynewstate:

  *++yyssp = yystate;

  if (yyssp >= yyss + yystacksize - 1)
    {
      YYSTYPE *yyvs1 = yyvs;
      short *yyss1 = yyss;
#ifdef YYLSP_NEEDED
      YYLTYPE *yyls1 = yyls;
#endif
      int size = yyssp - yyss + 1;

#ifdef yyoverflow

#ifdef YYLSP_NEEDED
      yyoverflow("parser stack overflow",
		 &yyss1, size * sizeof (*yyssp),
		 &yyvs1, size * sizeof (*yyvsp),
		 &yyls1, size * sizeof (*yylsp),
		 &yystacksize);
#else
      yyoverflow("parser stack overflow",
		 &yyss1, size * sizeof (*yyssp),
		 &yyvs1, size * sizeof (*yyvsp),
		 &yystacksize);
#endif

      yyss = yyss1; yyvs = yyvs1;
#ifdef YYLSP_NEEDED
      yyls = yyls1;
#endif
#else /* no yyoverflow */
      if (yystacksize >= YYMAXDEPTH)
	{
	  yyerror("parser stack overflow");
	  return 2;
	}
      yystacksize *= 2;
      if (yystacksize > YYMAXDEPTH)
	yystacksize = YYMAXDEPTH;
      yyss = (short *) alloca (yystacksize * sizeof (*yyssp));
      __yy_memcpy ((char *)yyss1, (char *)yyss, size * sizeof (*yyssp));
      yyvs = (YYSTYPE *) alloca (yystacksize * sizeof (*yyvsp));
      __yy_memcpy ((char *)yyvs1, (char *)yyvs, size * sizeof (*yyvsp));
#ifdef YYLSP_NEEDED
      yyls = (YYLTYPE *) alloca (yystacksize * sizeof (*yylsp));
      __yy_memcpy ((char *)yyls1, (char *)yyls, size * sizeof (*yylsp));
#endif
#endif

      yyssp = yyss + size - 1;
      yyvsp = yyvs + size - 1;
#ifdef YYLSP_NEEDED
      yylsp = yyls + size - 1;
#endif

#if YYDEBUG != 0
      if (yydebug)
	fprintf(stderr, "Stack size increased to %d\n", yystacksize);
#endif

      if (yyssp >= yyss + yystacksize - 1)
	YYABORT;
    }

#if YYDEBUG != 0
  if (yydebug)
    fprintf(stderr, "Entering state %d\n", yystate);
#endif

  goto yybackup;
 yybackup:

  yyn = yypact[yystate];
  if (yyn == YYFLAG)
    goto yydefault;

  if (yychar == YYEMPTY)
    {
#if YYDEBUG != 0
      if (yydebug)
	fprintf(stderr, "Reading a token: ");
#endif
      yychar = YYLEX;
    }


  if (yychar <= 0)		/* This means end of input. */
    {
      yychar1 = 0;
      yychar = YYEOF;		/* Don't call YYLEX any more */

#if YYDEBUG != 0
      if (yydebug)
	fprintf(stderr, "Now at end of input.\n");
#endif
    }
  else
    {
      yychar1 = YYTRANSLATE(yychar);

#if YYDEBUG != 0
      if (yydebug)
	{
	  fprintf (stderr, "Next token is %d (%s", yychar, yytname[yychar1]);
#ifdef YYPRINT
	  YYPRINT (stderr, yychar, yylval);
#endif
	  fprintf (stderr, ")\n");
	}
#endif
    }

  yyn += yychar1;
  if (yyn < 0 || yyn > YYLAST || yycheck[yyn] != yychar1)
    goto yydefault;

  yyn = yytable[yyn];

  if (yyn < 0)
    {
      if (yyn == YYFLAG)
	goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }
  else if (yyn == 0)
    goto yyerrlab;

  if (yyn == YYFINAL)
    YYACCEPT;

#if YYDEBUG != 0
  if (yydebug)
    fprintf(stderr, "Shifting token %d (%s), ", yychar, yytname[yychar1]);
#endif

  if (yychar != YYEOF)
    yychar = YYEMPTY;

  *++yyvsp = yylval;
#ifdef YYLSP_NEEDED
  *++yylsp = yylloc;
#endif

  if (yyerrstatus) yyerrstatus--;

  yystate = yyn;
  goto yynewstate;

yydefault:

  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;

yyreduce:
  yylen = yyr2[yyn];
  if (yylen > 0)
    yyval = yyvsp[1-yylen]; /* implement default value of the action */

#if YYDEBUG != 0
  if (yydebug)
    {
      int i;

      fprintf (stderr, "Reducing via rule %d (line %d), ",
	       yyn, yyrline[yyn]);
      for (i = yyprhs[yyn]; yyrhs[i] > 0; i++)
	fprintf (stderr, "%s ", yytname[yyrhs[i]]);
      fprintf (stderr, " -> %s\n", yytname[yyr1[yyn]]);
    }
#endif


  switch (yyn) {

case 20:
#line 94 "grammar.y"
{ doc.AddOpeningTag(*((std::string*)(yyvsp[-2].string_t)),*((std::list<Attribute>*)(yyvsp[-1].string_t)), yylineno, g_LexerColumn); ;
    break;}
case 21:
#line 95 "grammar.y"
{ doc.AddOpeningTag(*((std::string*)(yyvsp[-1].string_t)), yylineno, g_LexerColumn); ;
    break;}
case 22:
#line 99 "grammar.y"
{ doc.AddClosingTag(*((std::string*)(yyvsp[-1].string_t)), yylineno, g_LexerColumn); ;
    break;}
case 23:
#line 100 "grammar.y"
{ doc.AddClosingTag(*((std::string*)(yyvsp[-2].string_t)), yylineno, g_LexerColumn); ;
    break;}
case 24:
#line 104 "grammar.y"
{ doc.AddSingleTag(*((std::string*)(yyvsp[-3].string_t)), yylineno, g_LexerColumn); ;
    break;}
case 25:
#line 105 "grammar.y"
{ doc.AddSingleTag(*((std::string*)(yyvsp[-2].string_t)), yylineno, g_LexerColumn); ;
    break;}
case 26:
#line 109 "grammar.y"
{ (yyval.string_t) = gen_new_attr_list(((Attribute*)(yyvsp[0].string_t))); ;
    break;}
case 27:
#line 110 "grammar.y"
{ (yyval.string_t) = append_attr_in_list(((Attribute*)(yyvsp[0].string_t)), ((std::list<Attribute>*)(yyvsp[-1].string_t)));  ;
    break;}
case 28:
#line 114 "grammar.y"
{ (yyval.string_t) = gen_new_attr(*((std::string*)(yyvsp[-2].string_t)), *((std::string*)(yyvsp[0].string_t))); ;
    break;}
case 29:
#line 115 "grammar.y"
{ (yyval.string_t) = gen_new_attr(*((std::string*)(yyvsp[0].string_t))); ;
    break;}
case 32:
#line 121 "grammar.y"
{ return yyerror("Unsupported attribute value without quotes (single or double)"); ;
    break;}
case 33:
#line 122 "grammar.y"
{ return yyerror("Unsupported attribute value without quotes (single or double)"); ;
    break;}
}

#line 487 "bison.simple"

  yyvsp -= yylen;
  yyssp -= yylen;
#ifdef YYLSP_NEEDED
  yylsp -= yylen;
#endif

#if YYDEBUG != 0
  if (yydebug)
    {
      short *ssp1 = yyss - 1;
      fprintf (stderr, "state stack now");
      while (ssp1 != yyssp)
	fprintf (stderr, " %d", *++ssp1);
      fprintf (stderr, "\n");
    }
#endif

  *++yyvsp = yyval;

#ifdef YYLSP_NEEDED
  yylsp++;
  if (yylen == 0)
    {
      yylsp->first_line = yylloc.first_line;
      yylsp->first_column = yylloc.first_column;
      yylsp->last_line = (yylsp-1)->last_line;
      yylsp->last_column = (yylsp-1)->last_column;
      yylsp->text = 0;
    }
  else
    {
      yylsp->last_line = (yylsp+yylen-1)->last_line;
      yylsp->last_column = (yylsp+yylen-1)->last_column;
    }
#endif

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTBASE] + *yyssp;
  if (yystate >= 0 && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTBASE];

  goto yynewstate;

yyerrlab:

  if (! yyerrstatus)
    {
      ++yynerrs;

#ifdef YYERROR_VERBOSE
      yyn = yypact[yystate];

      if (yyn > YYFLAG && yyn < YYLAST)
	{
	  int size = 0;
	  char *msg;
	  int x, count;

	  count = 0;
	  for (x = (yyn < 0 ? -yyn : 0);
	       x < (sizeof(yytname) / sizeof(char *)); x++)
	    if (yycheck[x + yyn] == x)
	      size += strlen(yytname[x]) + 15, count++;
	  msg = (char *) malloc(size + 15);
	  if (msg != 0)
	    {
	      strcpy(msg, "parse error");

	      if (count < 5)
		{
		  count = 0;
		  for (x = (yyn < 0 ? -yyn : 0);
		       x < (sizeof(yytname) / sizeof(char *)); x++)
		    if (yycheck[x + yyn] == x)
		      {
			strcat(msg, count == 0 ? ", expecting `" : " or `");
			strcat(msg, yytname[x]);
			strcat(msg, "'");
			count++;
		      }
		}
	      yyerror(msg);
	      free(msg);
	    }
	  else
	    yyerror ("parse error; also virtual memory exceeded");
	}
      else
#endif
	yyerror("parse error");
    }

  goto yyerrlab1;
yyerrlab1:

  if (yyerrstatus == 3)
    {

      if (yychar == YYEOF)
	YYABORT;

#if YYDEBUG != 0
      if (yydebug)
	fprintf(stderr, "Discarding token %d (%s).\n", yychar, yytname[yychar1]);
#endif

      yychar = YYEMPTY;
    }

  /* Else will try to reuse lookahead token
     after shifting the error token.  */

  yyerrstatus = 3;		/* Each real token shifted decrements this */

  goto yyerrhandle;

yyerrdefault:  /* current state does not do anything special for the error token. */

#if 0
  /* This is wrong; only states that explicitly want error tokens
     should shift them.  */
  yyn = yydefact[yystate];  /* If its default is to accept any token, ok.  Otherwise pop it.*/
  if (yyn) goto yydefault;
#endif

yyerrpop:   /* pop the current state because it cannot handle the error token */

  if (yyssp == yyss) YYABORT;
  yyvsp--;
  yystate = *--yyssp;
#ifdef YYLSP_NEEDED
  yylsp--;
#endif

#if YYDEBUG != 0
  if (yydebug)
    {
      short *ssp1 = yyss - 1;
      fprintf (stderr, "Error: state stack now");
      while (ssp1 != yyssp)
	fprintf (stderr, " %d", *++ssp1);
      fprintf (stderr, "\n");
    }
#endif

yyerrhandle:

  yyn = yypact[yystate];
  if (yyn == YYFLAG)
    goto yyerrdefault;

  yyn += YYTERROR;
  if (yyn < 0 || yyn > YYLAST || yycheck[yyn] != YYTERROR)
    goto yyerrdefault;

  yyn = yytable[yyn];
  if (yyn < 0)
    {
      if (yyn == YYFLAG)
	goto yyerrpop;
      yyn = -yyn;
      goto yyreduce;
    }
  else if (yyn == 0)
    goto yyerrpop;

  if (yyn == YYFINAL)
    YYACCEPT;

#if YYDEBUG != 0
  if (yydebug)
    fprintf(stderr, "Shifting error token, ");
#endif

  *++yyvsp = yylval;
#ifdef YYLSP_NEEDED
  *++yylsp = yylloc;
#endif

  yystate = yyn;
  goto yynewstate;
}
#line 137 "grammar.y"
