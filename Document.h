#pragma once

#include "tag.h"
#include "debug.h"
#include <vector>
#include <string>
#include <map>
#include <stack>

using namespace std;

class Document
{
private:

	stack<Tag> state_stack;
	db_attr_type available_attributes;
	db_tag_type available_tags;

	bool html_was = false;
	bool title_was = false;
	
	void check_previous_state(Tag tag, int line, int column);

public:
	Document(db_attr_type attr_db, db_tag_type tag_db) : available_attributes(attr_db), available_tags(tag_db) {}
	~Document() {}

	void AddOpeningTag(string tag_name, int line, int column);
	void AddOpeningTag(string tag_name, list<Attribute> attrs, int line, int column);
	void AddClosingTag(string tag_name, int line, int column);
	void AddSingleTag(string tag_name, int line, int column);
	void AddSingleTag(string tag_name, list<Attribute> attrs, int line, int column);
};

static vector<string> HeadTagsAllowded = { "base" , "basefont" , "bgsound", "link", "meta", "script", "style", "title" };
static vector<string> TagsWithNoClosing = { "area", "base", "br", "col", "command", "embed", "hr", "img", "input", "keygen", "link", "meta", "param", "source", "track", "wbr" };
static vector<string> TagsCanBeNonClosed =
{
	"area"         ,
	"base"         ,
	"base"         ,
	"bgso"         ,
	"body"         ,
	"br"           ,
	"col"          ,
	"cofontlg"	   ,
	"coundmm"	   ,
	"dd"		   ,
	"dt"		   ,
	"embe"		   ,
	"frroupame"	   ,
	"heandad"	   ,
	"html"		   ,
	"img"		   ,
	"indput"	   ,
	"isindex"	   ,
	"keygen"	   ,
	"li"		   ,
	"link"		   ,
	"meta"		   ,
	"option"	   ,
	"p"			   ,
	"param"		   ,
	"plaintext"	   ,
	"rp"		   ,
	"rt"		   ,
	"source"	   ,
	"spacer"	   ,
	"tbody"		   ,
	"td"		   ,
	"tfoot"		   ,
	"th"		   ,
	"thead"		   ,
	"tr"		   ,
	"track"		   ,
	"wbr"		   ,
};
static map<string, pair<string, bool>> PrecedenceTags =
{
  { "area"      , { "map"      , false }   },
  { "base"      , { "head"     , false }   },
  { "bgsound"   , { "head"     , false }   },
  { "button"    , { "form"     , false }   },
  { "caption"   , { "table"    , true  }   },
  { "colgroup"  , { "table"    , false }   },
  { "command"   , { "menu"     , false }   },
  { "comment"   , { "body"     , false }   },
  { "dd"        , { "dl"       , false }   },
  { "dt"        , { "dl"       , false }   },
  { "fieldset"  , { "form"     , false }   },
  { "figcaption", { "figure"   , true  }   },
  { "frame"     , { "frameset" , false }   },
  { "isindex"   , { "head"     , false }   },
  { "keygen"    , { "form"     , false }   },
  { "legend"    , { "fieldset" , false }   },
  { "li"        , { "ul"       , false }   },
  { "link"      , { "head"     , false }   },
  { "meta"      , { "head"     , false }   },
  { "noframes"  , { "frameset" , false }   },
  { "optgroup"  , { "select"   , false }   },
  { "option"    , { "select"   , false }   },
  { "rp"        , { "ruby"     , false }   },
  { "rt"        , { "ruby"     , false }   },
  { "source"    , { "audio"    , false }   },
  { "style"     , { "head"     , false }   },
  { "summary"   , { "details"  , true  }   },
  { "tbody"     , { "table"    , false }   },
  { "td"        , { "tr"       , false }   },
  { "tfoot"     , { "table"    , false }   },
  { "th"        , { "tr"       , false }   },
  { "thead"     , { "table"    , true  }   },
  { "title"     , { "head"     , false }   },
  { "tr"        , { "table"    , false }   }
};