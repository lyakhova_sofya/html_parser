#include "grammar.cpp.h"
#include "lexer.h"

#include "attribute.h"
#include "tag.h"
#include "document.h"
#include "html_attr_and_tag.h"

#pragma warning( disable:4996 )
#pragma warning( disable:5033 )

using namespace std;

extern FILE* yyin;
extern int yyparse();
int yylex();

extern int yylineno;
extern int g_LexerColumn;

int yyerror(const char * err)
{
	string error_message(err);

	cout << endl << "Parser stopped with fatal syntax error." << endl;
	cout << "line: " << yylineno << "; " << "column: " << g_LexerColumn << endl;
	cout << "reason: " << err << endl;
	
	return -1;
}

Document doc(ATTR_DATABASE, TAG_DB);

int main(int argc, char * argv[])
{
	setlocale(LC_ALL, "Russian");
	if (argc < 2)
	{
		cout << "Invalid arguments. Usage: " << argv[0] << " <filename>" << endl;
	}

	yyin = fopen(argv[1], "r");
	if (!yyin) 
	{ 
		cout << "Error opening file!" << endl; 
	}
	yyparse();
	
	system("pause");
	return 0;
}