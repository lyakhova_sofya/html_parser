#include "document.h"
#include "utils.h"
#include "debug.h"
#include <list>

using namespace std;

extern int yylineno;
extern int g_LexerColumn;

string str_tolower(string s)
{
	transform(s.begin(), s.end(), s.begin(), [](unsigned char c) { return tolower(c); });
	return s;
}

bool tag_can_not_be_closed(string name)
{
	return (find(TagsCanBeNonClosed.begin(), TagsCanBeNonClosed.end(), name) != TagsCanBeNonClosed.end());
}

void Document::AddOpeningTag(string tag_name, int line, int column)
{
	tag_name = str_tolower(tag_name);
	if (tag_name == "html") 
	{ 
		this->html_was = true; 
	}
	if (tag_name == "title") 
	{ 
		this->title_was = true;
	}

	Tag tag(tag_name, this->available_attributes, this->available_tags, line, column);
	check_previous_state(tag, line, column);
	if (find(TagsWithNoClosing.begin(), TagsWithNoClosing.end(), tag_name) == TagsWithNoClosing.end())
	{
		this->state_stack.push(tag);
	}
	clean_structures();
}

void Document::AddOpeningTag(string tag_name, list<Attribute> attrs, int line, int column)
{
	tag_name = str_tolower(tag_name);

	if (tag_name == "html") 
	{ 
		this->html_was = true; 
	}
	if (tag_name == "title") 
	{
		this->title_was = true; 
	}

	Tag tag(tag_name, this->available_attributes, this->available_tags, line, column);

	for (auto it : attrs) 
	{ 
		tag.AddAttribute(it.get_name(), it.get_value()); 
	}
	check_previous_state(tag, line, column);

	if (find(TagsWithNoClosing.begin(), TagsWithNoClosing.end(), tag_name) == TagsWithNoClosing.end()) 
	{ 
		this->state_stack.push(tag);
	}
	clean_structures();
}

void Document::AddClosingTag(string tag_name, int line, int column)
{
	tag_name = str_tolower(tag_name);

	if (find(TagsWithNoClosing.begin(), TagsWithNoClosing.end(), tag_name) != TagsWithNoClosing.end())
	{
		return;
	}
	string previous_tag_name;
	
	while (true)
	{
		if (!state_stack.empty())
		{
			previous_tag_name = this->state_stack.top().get_name();
			if (tag_name != previous_tag_name && !tag_can_not_be_closed(previous_tag_name))
			{
				ERROR_MESSAGE("Unequal closing tag <" + tag_name + "> for <" + previous_tag_name + ">", ERROR_CODE_TAG, line, column);
				this->state_stack.pop();
				break;
			}
			else
			{
				this->state_stack.pop();
				break;
			}
		}
		else
		{
			break;
		}
	}
	clean_structures();
}

void Document::AddSingleTag(string tag_name, int line, int column)
{
	tag_name = str_tolower(tag_name);
	if (tag_name == "html") 
	{ 
		this->html_was = true;
	}
	if (tag_name == "title") 
	{ 
		this->title_was = true; 
	}

	Tag tag(tag_name, this->available_attributes, this->available_tags, line, column);
	check_previous_state(tag, line, column);
	clean_structures();
}

void Document::AddSingleTag(string tag_name, list<Attribute> attrs, int line, int column)
{
	tag_name = str_tolower(tag_name);

	Tag tag(tag_name, this->available_attributes, this->available_tags, line, column);

	for (auto it : attrs) 
	{
		tag.AddAttribute(it.get_name(), it.get_value()); 
	}

	check_previous_state(tag, line, column);
	clean_structures();
}

void Document::check_previous_state(Tag tag, int line, int column)
{
	stack<Tag> current_state_of_doc(this->state_stack);

	bool html_found = false;
	bool precedence_correct = false;
	bool precedence_is_required_to_check = false;
	bool precedence_is_strict = false;
	bool allowded_to_be_inside_head = false;
	bool bad_head_found = false;

	string precedence_tag;

	if (PrecedenceTags.find(tag.get_name()) != PrecedenceTags.end()) 
	{
		precedence_is_required_to_check = true;
		precedence_tag = PrecedenceTags.find(tag.get_name())->second.first;
		precedence_is_strict = PrecedenceTags.find(tag.get_name())->second.second;
	}
	
	if (find(HeadTagsAllowded.begin(), HeadTagsAllowded.end(), tag.get_name()) != HeadTagsAllowded.end())
	{
		allowded_to_be_inside_head = true;
	}

	if (precedence_is_strict && precedence_tag != current_state_of_doc.top().get_name()) 
	{
		ERROR_MESSAGE("Tag <" + tag.get_name() + "> " + "must be strictly inside element <" + precedence_tag + ">", ERROR_CODE_DOCUMENT, line, column);
	}
	else
	{
		while (!current_state_of_doc.empty())
		{
			if (precedence_is_required_to_check)
			{
				if (precedence_tag == current_state_of_doc.top().get_name())                   
				{ 
					precedence_correct = true; 
				}
			}
			if ("html" == current_state_of_doc.top().get_name())                                
			{ 
				html_found = true; 
			}
			if ("head" == current_state_of_doc.top().get_name() && !allowded_to_be_inside_head) 
			{ 
				bad_head_found = true;
			}
			current_state_of_doc.pop();
		}
		if (!precedence_correct && precedence_is_required_to_check)
		{
			ERROR_MESSAGE("Tag <" + tag.get_name() + "> " + "must be strictly inside element <" + precedence_tag + ">", ERROR_CODE_DOCUMENT, line, column);
		}
		if (!html_found && tag.get_name() != "html")
		{
			ERROR_MESSAGE("Tag <" + tag.get_name() + "> " + "must be inside element <html>", ERROR_CODE_DOCUMENT, line, column);
		}
		if (bad_head_found)
		{
			ERROR_MESSAGE("Tag <" + tag.get_name() + "> " + "can not be inside element <head>", ERROR_CODE_DOCUMENT, line, column);
		}
		return;
	}
}